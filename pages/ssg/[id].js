import { getAllNoteIds, getNoteData } from '../../lib/notes-util'
import Head from 'next/head'
import Link from 'next/link'
import styles from '../../styles/Home.module.css'

export async function getStaticPaths() {
  const paths = getAllNoteIds()
  return {
    paths,
    fallback: false,
  }
}

export async function getStaticProps({ params }) {
  const noteData = await getNoteData(params.id)
  return {
    props: {
      noteData,
    },
  }
}

export default function Post({ noteData }) {
  return (
    <div className={styles.container}>
      <Head>
        <title>{noteData.title}</title>
      </Head>
      <main className={styles.main}>
        <article>
          <h1 className={styles.title}>{noteData.title}</h1>
          <div>
            <Date dateString={noteData.date} />
          </div>
          <div dangerouslySetInnerHTML={{ __html: noteData.contentHtml }} />
        </article>
        <Link href='/'>
          <a className={styles.card}>Go Home</a>
        </Link>
      </main>
    </div>
  )
}
