import Head from 'next/head'
import Link from 'next/link'
import styles from '../styles/Home.module.css'

function Ssr({ imageUrl }) {
  console.log('url', imageUrl)
  return (
    <div className={styles.container}>
      <Head>
        <title>Server Side Rendered Page</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>Server Side Rendered Page</h1>
        <img src={imageUrl} />
        <Link href='/'>
          <a className={styles.card}>Go Home</a>
        </Link>
      </main>
    </div>
  )
}

Ssr.getInitialProps = async (ctx) => {
  const res = await fetch('https://source.unsplash.com/random/400x300')
  return { imageUrl: res.url }
}

export default Ssr
