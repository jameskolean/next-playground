import { useState } from 'react'
import Head from 'next/head'
import Link from 'next/link'
import styles from '../styles/Home.module.css'
import { getSortedNotesData } from '../lib/notes-util'

export async function getStaticProps() {
  const allNotesData = getSortedNotesData()
  return {
    props: {
      allNotesData,
    },
  }
}

export default function Home({ allNotesData }) {
  const [isrPage, setIsrPage] = useState('somepage')
  const handleChange = (event) => {
    const val = event.target.value
    setIsrPage(val.replace(' ', '-').replace('--', '-'))
  }
  const handleSubmit = (event) => {
    event.preventDefault()
    window.location.href = `/isr/${isrPage}`
  }

  return (
    <div className={styles.container}>
      <Head>
        <title>NEXT.js Playgropund</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>Welcome to Next.js playground</h1>

        <div className={styles.grid}>
          <div className={styles.card}>
            <h3>Static Site Generateration (SSG)</h3>
            <p>
              Here is an example page using static generation. We also show off
              dynamic routes because it's pretty crucial for this type of
              renderting.
            </p>
            <ul className={styles.list}>
              {allNotesData.map(({ id, date, title }) => (
                <li className={styles.listItem} key={id}>
                  <Link href={`ssg/${id}`}>
                    <a>{title} &rarr;</a>
                  </Link>
                </li>
              ))}
            </ul>
          </div>

          <Link href='ssr'>
            <a className={styles.card}>
              <h3>Server Side Rendering (SSR)&rarr;</h3>
              <p>Here is an example page being rendered on the server.</p>
            </a>
          </Link>

          <Link href='csr'>
            <a className={styles.card}>
              <h3>Client Side Rendering (CSR)&rarr;</h3>
              <p>
                Here is an example page with componets rendered by the client.
              </p>
            </a>
          </Link>

          <div className={styles.card}>
            <h3>Incremental Static Regeneration (ISR)</h3>
            <p>
              Here is an example page that get statically generated ehrn a
              client accessing it.
            </p>
            <form onSubmit={handleSubmit}>
              <label>
                Make up a page:{' '}
                <input type='text' value={isrPage} onChange={handleChange} />
              </label>
              <input type='submit' value='Go &rarr;' />
            </form>
          </div>

          <Link href='lambda'>
            <a className={styles.card}>
              <h3>Lambda&rarr;</h3>
              <p>Let's call a lambda function from the client.</p>
            </a>
          </Link>
        </div>
      </main>
    </div>
  )
}
