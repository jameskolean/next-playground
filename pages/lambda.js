import { useEffect, useState } from 'react'
import Head from 'next/head'
import Link from 'next/link'
import styles from '../styles/Home.module.css'

export default function Lambda() {
  const [greeting, setGreeting] = useState('Unset')
  useEffect(() => {
    const callLamdba = async () => {
      const res = await fetch('api/hello')
      const data = await res.json()
      setGreeting(data.greeting)
    }
    callLamdba()
  }, [])
  return (
    <div className={styles.container}>
      <Head>
        <title>Lambda Page</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>Lamdba Page</h1>
        <div className={styles.description}>
          Lambda function says: {greeting}
        </div>
        <Link href='/'>
          <a className={styles.card}>Go Home</a>
        </Link>
      </main>
    </div>
  )
}
