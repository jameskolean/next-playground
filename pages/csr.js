import Head from 'next/head'
import Link from 'next/link'
import useSound from 'use-sound'
import styles from '../styles/Home.module.css'

export default function Csr() {
  const [playLaserBlast, { stop }] = useSound('/sounds/laser.mp3', {
    volume: 0.5,
  })
  return (
    <div className={styles.container}>
      <Head>
        <title>Client Side Generated Page</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>Client Side Rendered Page</h1>
        <button onClick={playLaserBlast}>Shoot Lasers</button>
        <Link href='/'>
          <a className={styles.card}>Go Home</a>
        </Link>
      </main>
    </div>
  )
}
