import Head from 'next/head'
import Link from 'next/link'
import styles from '../../styles/Home.module.css'
import { useRouter } from 'next/router'
import { parseISO, format } from 'date-fns'

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: true,
  }
}

export async function getStaticProps({ params }) {
  await new Promise((res) => setTimeout(res, 500))
  const noteData = { ...params, title: 'Hello', date: new Date().toString() }
  return {
    props: noteData,
    revalidate: 30,
  }
}

export default function Post(noteData) {
  const router = useRouter()
  if (router.isFallback) {
    return <div> Loading ....</div>
  }
  return (
    <div className={styles.container}>
      <Head>
        <title>{noteData.title}</title>
      </Head>
      <main className={styles.main}>
        <article>
          <h1 className={styles.title}>{noteData.title}</h1>
          <div>page created {noteData.date.toString()}</div>
          <div>in 30 seconds we will revalidate.</div>
          <br />
        </article>
        <Link href='/'>
          <a className={styles.card}>Go Home</a>
        </Link>
      </main>
    </div>
  )
}
